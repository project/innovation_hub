## INTRODUCTION

This module is a proof of concept to eventually be part of Drupal.org.

## FEATURES

- Community-Driven Evaluation: Unlike traditional platforms that rely on a select jury to decide the fate of new ideas, the Community Innovation Hub empowers the Drupal community to vote on, discuss, and contribute to proposed innovations. This feature fosters a more inclusive and democratic process for idea validation and development.
- Visibility and Traction Tracking: The module features an intuitive dashboard that highlights initiatives gaining the most interest and traction within the community. This visibility ensures that promising ideas receive the attention and resources they need to mature into viable projects.
- Collaborative Innovation Space: A dedicated space for initiative owners to present their ideas, gather feedback, and collaborate with community members. This feature streamlines the feedback loop, enabling owners to refine their proposals and rally community support more effectively.
- Resource Aggregation and Funding: By identifying and showcasing initiatives with high community interest, the module facilitates the concentration of resources and funding towards the most impactful projects. This mechanism helps in better allocating the community's collective resources towards shared goals.
- Innovation Tracking and Progress Updates: Initiative owners can regularly update the community on their project's progress, ensuring transparency and maintaining momentum. This feature allows the community to stay informed about ongoing developments and contributes to a sense of collective achievement.
- Technical Evaluation Tools: The module provides tools for technical evaluators to assess the innovation pulse of the community. This includes metrics on participation, interest levels, and the technological viability of proposed initiatives, enabling a data-driven approach to support innovation.

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## DEPENDECIES
- Drupal 10
- Open Collective
- Paragraphs

## MAINTAINERS

Current maintainers for Drupal 10:

- Ricardo Marcelino (rfmarcelino) - https://www.drupal.org/u/rfmarcelino
